
const express = require('express');
const app = express();
const port = 8000;
var bodyParser = require("body-parser");
app.use(bodyParser.json());
const routes = require('./routes/routes.js')
const routeAction = new routes(app);
routeAction.init()

app.listen(port,()=>{
    console.log(new Date(),`Server Running and Listeing at Port ${port}`)
})