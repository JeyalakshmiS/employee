const mongoose = require('mongoose')
const mongoConnection = "mongodb://localhost:27017/trial";
mongoose.connect(mongoConnection,{useNewUrlParser: true,useUnifiedTopology: true})
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));
mongoose.set('useFindAndModify', false);

const EmployeeService = function (app){

    this.app = app;
}
module.exports = EmployeeService;

EmployeeService.prototype.find = function (input){
    console.log("Input",input);

    let schema = mongoose.Schema({
        name:String
    })
    let employee = mongoose.model("employees",schema)

    return employee.findOne({name:input.name},(err,result)=>{
        if (err) return handleError(err);
        console.log(result)
    })
}

EmployeeService.prototype.update = function (input){

    console.log("Update data from db",input)
    let schema = mongoose.Schema({
        name :{type: String ,required:true}
    })
    let employee = mongoose.model("employees",schema)

    return employee.findOneAndUpdate({name:input.name},{$set:{age:23}},{upsert:true},(err,result)=>{
        if (err) return handleError(err);
        console.log(result)
    })
}

EmployeeService.prototype.insert= function (input){

    console.log("insert",input)

    var employeeSchema = mongoose.Schema({
        name: String,
        age : Number,
        email: String,
        address: String,
        mobileNumber: Number

    });

    const employeeData = mongoose.model('employees', employeeSchema, 'employees');

    const employeeInfo = new employeeData({
        name: input.name,
        age : input.age,
        email: input.email,
        address: input.address,
        mobileNumber: input.mobileNumber
    });
    return mongoose.connection.collection('employees').insertOne(employeeInfo);

}


EmployeeService.prototype.delete = function (input){

    console.log("Update data from db",input)
    let schema = mongoose.Schema({
        name :{type: String}
    })
    let employee = mongoose.model("employees",schema)

    return employee.deleteOne({name:input.name},(err,result)=>{
        if (err) return handleError(err);
        console.log(result)
    })
}

