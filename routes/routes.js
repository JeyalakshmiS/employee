const apiAction = require('../action/employeeAction')

const RouteAction = function (app){
    this.app = app;
    this.apiActionInstance = new apiAction(app)
}

module.exports = RouteAction;

RouteAction.prototype.init = function (){

    this.app.get("/",(req,res)=>{
        res.send({'test': 'Ok'})
    })

    this.app.post("/employeeCreation",(req,res)=>{
        console.log(new Date(), "Employee Cretion Call",req.body)
        this.apiActionInstance.employeeCreation(req.body)
            .then(function (finalResult) {
                console.log("Final response for Employee Creation ", finalResult);
                res.send(finalResult);
            })
            .catch(function (e) {
                console.log("Catch handler Employee Creation" + e);
                res.send(e.message);
            });
    })

    this.app.get("/getEmployeeDetails",(req,res)=>{
        console.log(new Date(), "Employee Details",req.query)
        this.apiActionInstance.getEmployeeDetails(req.query)
            .then(function (finalResult) {
                console.log("Final response for getEmployeeDetails ", finalResult);
                res.send(finalResult);
            })

            .catch(function (e) {
                console.log("Catch handler getEmployeeDetails" + e);
                res.send(e.message);
            });
    })

    this.app.get("/updateEmployeeDetails",(req,res)=>{
        console.log(new Date(), "updateEmployeeDetails",req.query)
        this.apiActionInstance.updateEmployeeDetails(req.query)
            .then(function (finalResult) {
                console.log("Final response for updateEmployeeDetails ", finalResult);
                res.send(finalResult);
            })

            .catch(function (e) {
                console.log("Catch handler updateEmployeeDetails" + e);
                res.send(e.message);
            });
    })

    this.app.get("/deleteEmployeeDetails",(req,res)=>{
        console.log(new Date(), "Delete Employee Details",req.query)
        this.apiActionInstance.deleteEmployeeDetails(req.query)
            .then(function (finalResult) {

                console.log("Final response for Delete Emplye ", finalResult);

                res.send(finalResult);
            })

            .catch(function (e) {
                console.log("Catch handler updateEmployeeDetails" + e);
                res.send(e.message);
            });
    })
}
