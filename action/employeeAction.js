const apiService = require('../service/employeeService')
const EmployeeAction =  function(app){
    this.app = app;
    this.apiServiceInstance = new apiService(app)
}
module.exports = EmployeeAction;

EmployeeAction.prototype.employeeCreation = function (input){

    console.log("Enter into the employeeCreation")
    let self =this;
    var response = {
        status: "FAILURE",
        err: {},
        data: {}
    };
    return new Promise(function (resolve, reject) {
        if (input) {
            return self.apiServiceInstance.insert(input)
                .then(function (result) {
                    console.log("result", result)
                    if (result != null) {
                        response['status'] = "SUCCESS";
                        response['data']['message'] = "Employee Cretion Successfully";
                        resolve(response)
                    } else {
                        response['err']['message'] = "Employee Cretion Unsuccessfully";
                        resolve(response)
                    }
                })
                .catch(function (e) {
                    console.log(e);
                    response['err']['message'] = e.message;
                    reject(response);
                })
        }
        else {
            response['err']['message'] = "Employee Cretion Unsuccessfully";
            resolve(response)
        }
    })
}

EmployeeAction.prototype.getEmployeeDetails = function (input) {

    var self = this;
    console.log("Input", input)


    var response = {
        status: "FAILURE",
        err: {},
        data: {}
    };
    return new Promise(function (resolve, reject) {
        if (input ) {
            return self.apiServiceInstance.find(input)
                .then(function (result) {
                    if (result != null) {
                        console.log("res", result)
                        response['status'] = "SUCCESS";
                        response['data']['message'] = "Employee Already Exists";
                        resolve(response)
                    } else {
                        response['err']['message'] = "Employee Not Exists Need to SignUp"
                        resolve(response)
                    }

                })
        }
    })
}
EmployeeAction.prototype.updateEmployeeDetails = function(input){
    var self = this;
    console.log("Input", input)


    var response = {
        status: "FAILURE",
        err: {},
        data: {}
    };
    return new Promise(function (resolve, reject) {
        if (input ) {
            return self.apiServiceInstance.update(input)
                .then(function (result) {
                    if (result != null) {
                        console.log("res", result)
                        response['status'] = "SUCCESS";
                        response['data']['message'] = "Employee Details Updted";
                        resolve(response)
                    } else {
                        response['err']['message'] = "Employee Details Not Updted";
                        resolve(response)
                    }

                })
        }
    })
}


EmployeeAction.prototype.deleteEmployeeDetails = function(input){
    var self = this;
    console.log("Input", input)


    var response = {
        status: "FAILURE",
        err: {},
        data: {}
    };
    return new Promise(function (resolve, reject) {
        if (input && input.name) {
            return self.apiServiceInstance.delete(input)
                .then(function (result) {
                    if (result != null) {
                        console.log("res", result)
                        response['status'] = "SUCCESS";
                        response['data']['message'] = "Deleted Successully";
                        resolve(response)
                    } else {
                        response['err']['message'] = "Deleted Unsuccessul"
                        resolve(response)
                    }

                })
        }
    })
}